<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    protected $table = "artistas";
    protected $fillable = ['nombre', 'apellido', 'img', 'redes_sociales'];


    public function artista_banda() {
      return $this->belongsToMany('App\Banda', 'artista_banda', 'id_banda', 'id_artista')->whereNull('deleted_at');
    }

    public function scopeBuscar($query, $request) {
        if ($request->id) {
            $query->where('id', $request->id);
        }
        if ($request->nombre) {
            $query->where('nombre', 'like', '%'.$request->nombre.'%');
        }
        if ($request->redes_sociales) {
            $query->where('redes_sociales', 'like', '%'.$request->redes_sociales.'%');
        }

        return $query;
    }
}
