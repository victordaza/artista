<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Artista;
use Illuminate\Http\Request;

class ArtistasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artistas = Artista::buscar($request)
        ->orderBy('id', 'Desc')
        ->paginate();

        return response()->json([
          'artistas' => $artistas
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errores = [];
        $alertas = [];
        $validator = Validator::make($request->all(),[
          'nombre' => 'required|max:50',
          'apellido' => 'required|max:50'
          ]);
        if ($validator->fails()) {
            return response()->json([
              'errores' => $validator->errors()->all(),
              'alertas' => $alertas
            ]);
        }
        DB::beginTransaction();
        try {
            $artista = new Artista();
            $artista->fill($request->all());
            $artista->save();
            DB::commit();
            $alertas[] = 'Se ha realizado la operación con exito.';
        } catch(\Exception $e) {
            DB::rollBack();
            $errores = $e->getMessage(). ' '.$e->getLine();
        }

        return response()->json([
          'alertas' => $alertas,
          'errores' => $errores
        ], 200);
    }

    public function update(Request $request, $id)
    {
      $errores = [];
      $alertas = [];
      $validator = Validator::make($request->all(),[
        'id_artista' => 'required|exists:artistas,id',
        'nombre' => 'required|max:50',
        'apellido' => 'required|max:50'
        ]);
      if ($validator->fails()) {
          return response()->json([
            'errores' => $validator->errors()->all(),
            'alertas' => $alertas
          ]);
      }
      DB::beginTransaction();
      try {
          $artista = Artista::where('id_artista', $request->id_artista)->first();
          $artista->fill($request->all());
          $artista->save();
          DB::commit();
          $alertas[] = 'Se ha realizado la operación con exito.';
      } catch(\Exception $e) {
          DB::rollBack();
          $errores = $e->getMessage(). ' '.$e->getLine();
      }

      return response()->json([
        'alertas' => $alertas,
        'errores' => $errores
      ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
