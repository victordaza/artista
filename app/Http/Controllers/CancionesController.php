<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Cancion;
use Illuminate\Http\Request;

class CancionesController extends Controller
{

  public function index()
  {
      $canciones = Cancion::buscar($request)
      ->orderBy('id', 'Desc')
      ->paginate();

      return response()->json([
        'canciones' => $canciones
      ], 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $errores = [];
      $alertas = [];
      $validator = Validator::make($request->all(),[
        'id_banda' => 'required|exists:bandas,id',
        'nombre' => 'required|max:50',
        'duracion' => 'required|time'
        ]);
      if ($validator->fails()) {
          return response()->json([
            'errores' => $validator->errors()->all(),
            'alertas' => $alertas
          ]);
      }
      DB::beginTransaction();
      try {
          $cancion = new Cancion();
          $cancion->fill($request->all());
          $cancion->save();
          DB::commit();
          $alertas[] = 'Se ha realizado la operación con exito.';
      } catch(\Exception $e) {
          DB::rollBack();
          $errores = $e->getMessage(). ' '.$e->getLine();
      }

      return response()->json([
        'alertas' => $alertas,
        'errores' => $errores
      ], 200);
  }

  public function update(Request $request, $id)
  {
    $errores = [];
    $alertas = [];
    $validator = Validator::make($request->all(),[
      'id_cancion' => 'required|exists:canciones,id',
      'id_banda' => 'required|exists:bandas,id',
      'nombre' => 'required|max:50',
      'duracion' => 'required|time'
      ]);
    if ($validator->fails()) {
        return response()->json([
          'errores' => $validator->errors()->all(),
          'alertas' => $alertas
        ]);
    }
    DB::beginTransaction();
    try {
        $cancion = Cancion::where('id_cancion', $request->id_cancion)->first();
        $cancion->fill($request->all());
        $cancion->save();
        DB::commit();
        $alertas[] = 'Se ha realizado la operación con exito.';
    } catch(\Exception $e) {
        DB::rollBack();
        $errores = $e->getMessage(). ' '.$e->getLine();
    }

    return response()->json([
      'alertas' => $alertas,
      'errores' => $errores
    ], 200);
  }
}
