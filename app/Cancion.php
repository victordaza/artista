<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancion extends Model
{
    protected $table = "canciones";
    protected $fillable = ['nombre', 'duracion', 'id_banda'];

    public function banda() {
        return $this->benlogsTo('App\Banda', 'id_banda', 'id');
    }

    public function scopeBuscar($query, $request) {
        if ($request->nombre) {
            $query->where('nombre', 'like', '%'.$request->nombre.'%');
        }

        return $query;
    }
}
