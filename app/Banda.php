<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banda extends Model
{
    protected $table = "bandas";
    protected $fillable = ['nombre'];

    public function banda_artista() {
        return $this->belongsToMany('App\Banda', 'artista_banda', 'id_artista', 'id_banda')->whereNull('deleted_at');
    }

    public function scopeBuscar($query, $request) {
        if ($request->nombre) {
            $query->where('nombre', 'like', '%'.$request->nombre.'%');
        }

        return $query;
    }
}
