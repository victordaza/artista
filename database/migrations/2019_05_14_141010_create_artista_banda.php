<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistaBanda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artista_banda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_artista')->unsigned()->nullable();
            $table->integer('id_banda')->unsigned()->nullable();
            $table->softDeletes();
            $table->foreign('id_artista')->references('id')->on('artistas');
            $table->foreign('id_banda')->references('id')->on('bandas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artista_banda');
    }
}
