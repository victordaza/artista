<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{name?}', function () {
    return view('welcome');
});


Route::group(['prefix' => 'artistas'], function() {
  Route::post('/', [
    'as' => 'artista.index',
    'uses' => 'ArtistasController@index'
  ]);
  Route::post('/store', [
    'as' => 'artista.store',
    'uses' => 'ArtistasController@store'
  ]);
  Route::post('/update', [
    'as' => 'artista.update',
    'uses' => 'ArtistasController@update'
  ]);
});

Route::group(['prefix' => 'bandas'], function() {
  Route::post('/', [
    'as' => 'banda.index',
    'uses' => 'BandasController@index'
  ]);
  Route::post('/store', [
    'as' => 'banda.store',
    'uses' => 'BandasController@store'
  ]);
  Route::post('/update', [
    'as' => 'banda.update',
    'uses' => 'BandasController@update'
  ]);
});


Route::group(['prefix' => 'canciones'], function() {
  Route::post('/', [
    'as' => 'cancion.index',
    'uses' => 'CancionesController@index'
  ]);
  Route::post('/store', [
    'as' => 'cancion.store',
    'uses' => 'CancionesController@store'
  ]);
  Route::post('/update', [
    'as' => 'cancion.update',
    'uses' => 'CancionesController@update'
  ]);
});
