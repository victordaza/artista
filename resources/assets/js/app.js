
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import vueRouter from 'vue-router';
import Artista from './Artista';
import Banda from './Banda';
import Cancion from './Cancion';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.use(vueRouter);
Vue.component('example', require('./components/Example.vue'));
Vue.component('pagination', require('laravel-vue-pagination'));

const routes = [
  { path: '/artistas', component: Artista },
  { path: '/bandas', component: Banda },
  { path: '/canciones', component: Cancion },

];

const router = new vueRouter({
  mode: 'history',
  routes: routes
})
const app = new Vue({
    el: '#vue-container',
    router
});
